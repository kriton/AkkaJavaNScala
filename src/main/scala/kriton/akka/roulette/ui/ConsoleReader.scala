package kriton.akka.roulette.ui

import scala.concurrent.duration.DurationInt

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.InputStream

class ConsoleReader(player: ActorRef) extends Actor {
  import context._
  import ConsoleReader._

  system.scheduler.schedule(100 milliseconds, 100 milliseconds, self, Input())

  val in: BufferedReader = new BufferedReader(new InputStreamReader(System.in));

  def receive = {
    case Input() if in.ready() => player ! in.readLine()
  }
}

object ConsoleReader {
  def props(player: ActorRef) = Props(new ConsoleReader(player))

  implicit def inputStreamWrapper(in: InputStream) = new BufferedReader(new InputStreamReader(in))

  case class Input()
}