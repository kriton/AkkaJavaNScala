package kriton.akka.roulette.messages

case class GetWinningBetsCommand(symbol: Int)