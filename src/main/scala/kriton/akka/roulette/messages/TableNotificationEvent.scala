package kriton.akka.roulette.messages

case class TableNotificationEvent(text: String)