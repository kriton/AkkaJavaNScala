package kriton.akka.roulette.messages

import akka.actor.ActorRef

case class Bet (symbol: Int, ammount: Int, better: ActorRef, name: String)