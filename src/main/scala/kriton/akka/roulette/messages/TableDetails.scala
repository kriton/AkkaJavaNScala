package kriton.akka.roulette.messages

import akka.actor.ActorRef

case class TableDetails(name: String, seatedPlayers: Int, ref: ActorRef)