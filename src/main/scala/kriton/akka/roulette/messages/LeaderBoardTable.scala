package kriton.akka.roulette.messages

case class LeaderBoardTable(sortedPlayerWinnings: String)