package kriton.akka.roulette.messages

case class SeatCommand(tableName: String, playerName: String = null)