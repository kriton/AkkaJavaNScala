package kriton.akka.roulette.messages

case class WinningNumber(number: Int)