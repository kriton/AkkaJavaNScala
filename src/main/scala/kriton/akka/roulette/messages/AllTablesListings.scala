package kriton.akka.roulette.messages

case class AllTablesListings(tables: java.util.Set[TableDetails])