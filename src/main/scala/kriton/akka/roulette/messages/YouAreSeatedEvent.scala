package kriton.akka.roulette.messages

import akka.actor.ActorRef

case class YouAreSeatedEvent(table: ActorRef)