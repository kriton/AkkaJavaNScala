package kriton.akka.roulette.messages

case class BetAcceptedEvent(bet: Bet)