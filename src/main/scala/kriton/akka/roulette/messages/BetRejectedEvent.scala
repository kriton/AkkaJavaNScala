package kriton.akka.roulette.messages

case class BetRejectedEvent(bet: Bet)