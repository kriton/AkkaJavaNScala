package kriton.akka.roulette.messages

case class WonEvent(amount: Int, bet: Bet)