package kriton.akka.roulette.messages

case class NewTableEvent(name: String)