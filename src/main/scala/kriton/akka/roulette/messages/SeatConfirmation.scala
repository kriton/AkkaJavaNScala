package kriton.akka.roulette.messages

case class SeatConfirmation(playerName: String)