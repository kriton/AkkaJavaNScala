package kriton.akka.roulette.domain

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.ActorLogging
import akka.actor.actorRef2Scala
import kriton.akka.roulette.messages.PrintCommand
import kriton.akka.roulette.messages.GetAllTablesCommand
import kriton.akka.roulette.messages.UnseatCommand
import kriton.akka.roulette.messages.BetCommand
import kriton.akka.roulette.messages.SeatCommand

class PlayerCommandInterpreter(player: ActorRef) extends Actor with ActorLogging {

  implicit class CaseInsensitiveRegex(sc: StringContext) {
    def ci = ("(?i)" + sc.parts.mkString).r
  }

  def receive = {
    case ci"bet (\d+)$ammount on (\d+)$symbol" => player ! BetCommand(symbol.toInt, ammount.toInt)
    case ci"show tables"                       => player ! GetAllTablesCommand()
    case ci"seat at (.+)$table"                => player ! SeatCommand(table)
    case ci"unseat"                            => player ! UnseatCommand()
    case ci"leaderboard"                       => player ! PrintCommand()
    case ci"crash"                             => player ! "crash"
    case _                                     => log.info("invaid command")
  }
}

object PlayerCommandInterpreter {
  def props(player: ActorRef) = Props(new PlayerCommandInterpreter(player))
}