package kriton.akka.roulette.domain;

import static akka.japi.pf.ReceiveBuilder.match;

import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

import kriton.akka.roulette.messages.BallLandedEvent;
import kriton.akka.roulette.messages.BettingFinishedEvent;
import kriton.akka.roulette.messages.NewGameStartingEvent;
import kriton.akka.roulette.messages.PickBallCommand;
import kriton.akka.roulette.messages.SpinCommand;
import kriton.akka.roulette.messages.WinningNumber;
import scala.PartialFunction;
import scala.concurrent.duration.FiniteDuration;
import scala.runtime.BoxedUnit;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public class Wheel extends AbstractLoggingActor {
	// http://doc.akka.io/docs/akka/2.4.7/java/lambda-actors.html#upgrade
	private PartialFunction<Object, BoxedUnit> ballIsSpinning;
	private PartialFunction<Object, BoxedUnit> ballLanded;
	private PartialFunction<Object, BoxedUnit> ballPicked;
	
	private ActorRef croupier;
	
	private Wheel(ActorRef table) {
		log().info("Wheel is ready!");

		ballPicked = match(SpinCommand.class, command -> {
			context().system().scheduler().scheduleOnce(
					new FiniteDuration(10L, TimeUnit.SECONDS), table, 
					new BettingFinishedEvent(), context().system().dispatcher(), self());
			context().system().scheduler().scheduleOnce(
					new FiniteDuration(20L, TimeUnit.SECONDS), self(), 
					new BallLandedEvent(generateRandomNumer()), context().system().dispatcher(), self());
			context().become(ballIsSpinning);
			croupier = sender();
			log().info("Wheel and Ball are spinning!");
		}).matchAny(this::unhandled).build();
		
		ballIsSpinning = match(BallLandedEvent.class, event -> {
			context().become(ballLanded);
			table.tell(new WinningNumber(event.symbol()), self());
			croupier.forward(event, context());
			log().info("Ball landed on {}!", event.symbol());
		}).matchAny(this::unhandled).build();
		
		ballLanded = match(PickBallCommand.class, command -> {
			table.tell(new NewGameStartingEvent(), self());
			context().become(ballPicked);
			log().info("Ball has been picked!");
		}).matchAny(this::unhandled).build();
		
		context().become(ballLanded);
	}
	
	private int generateRandomNumer() {
		return new SecureRandom().nextInt(37);
	}
	
	public static Props props(ActorRef table) {
		return Props.create(Wheel.class, () -> new Wheel(table));
	}
}
