package kriton.akka.roulette.domain;

import kriton.akka.roulette.messages.AllTablesListings;
import kriton.akka.roulette.messages.Bet;
import kriton.akka.roulette.messages.BetAcceptedEvent;
import kriton.akka.roulette.messages.BetCommand;
import kriton.akka.roulette.messages.BetRejectedEvent;
import kriton.akka.roulette.messages.GetAllTablesCommand;
import kriton.akka.roulette.messages.LeaderBoardTable;
import kriton.akka.roulette.messages.MoneyAdjustedEvent;
import kriton.akka.roulette.messages.PrintCommand;
import kriton.akka.roulette.messages.SeatCommand;
import kriton.akka.roulette.messages.SeatConfirmation;
import kriton.akka.roulette.messages.TableNotificationEvent;
import kriton.akka.roulette.messages.UnSeatConfirmation;
import kriton.akka.roulette.messages.UnseatCommand;
import kriton.akka.roulette.messages.WonEvent;
import kriton.akka.roulette.messages.YouAreSeatedEvent;
import kriton.akka.roulette.messages.YouAreUnSeatedEvent;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Procedure;
import akka.persistence.UntypedPersistentActor;

public class Player extends UntypedPersistentActor {
	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	private final ActorRef lobby;
	private final String name;
	private ActorRef table;
	private int money;

	private Player(String name, int startUpmoney, ActorRef lobby) {
		this.lobby = lobby;
		this.name = name;
		money = startUpmoney;
		log.info("{} is ready!", name);
	}

	public static Props props(String name, int startUpmoney, ActorRef lobby) {
		return Props.create(Player.class, () -> new Player(name, startUpmoney, lobby));
	}

	Procedure<Object> seated = new Procedure<Object>() {
	    @Override
	    public void apply(Object msg) {
	    	if (msg instanceof SeatCommand) {
				log.info("You must unseat yourself from the current table first!");
			} else if (msg instanceof UnseatCommand) {
				table.tell(new UnseatCommand(name), self());
			} else if (msg instanceof YouAreUnSeatedEvent) {
				persist(msg, event -> {
					log.info("You are back at the lobby!");
					table.tell(new UnSeatConfirmation(name), self());
					table = null;
					getContext().unbecome();
				});
			} else if (msg instanceof BetCommand) {
				BetCommand command = (BetCommand) msg;
				table.tell(new Bet(command.symbol(), command.ammount(), self(), name), self());
			} else {
				commonHandler(msg);
			}
	    }
	};

	private void commonHandler(Object msg) {
		if (msg instanceof TableNotificationEvent) {
			log.info(((TableNotificationEvent)msg).text());
		} else if (msg instanceof BetRejectedEvent) {
			log.info("Your bet was rejected! You have {} money left", money);
		} else if (msg instanceof BetAcceptedEvent) {
			persist(msg, event -> {
				money -= ((BetAcceptedEvent)event).bet().ammount();
				lobby.tell(new MoneyAdjustedEvent(name, money), self());
				log.info("Your bet was accepted! You have {} money left", money);
			});
		} else if (msg instanceof WonEvent) {
			persist(msg, event -> {
				money += ((WonEvent)event).amount();
				lobby.tell(new MoneyAdjustedEvent(name, money), self());
				log.info("You won {} from your bet on {}! You have {} money left", ((WonEvent)event).amount(),((WonEvent)event).bet().ammount(), money);
			});
		} else if (msg instanceof PrintCommand) {
			lobby.tell(msg, self());
		}  else if (msg instanceof LeaderBoardTable) {
			log.info("Leaderboard:\n{}", ((LeaderBoardTable)msg).sortedPlayerWinnings());
		} else if (msg.equals("crash")){
			throw new RuntimeException();
		} else {
			unhandled(msg);
		}	
	}

	@Override
	public String persistenceId() {
		return "player_" + name;
	}

	@Override
	public void onReceiveCommand(Object msg) throws Throwable {
		if (msg instanceof UnseatCommand) {
			log.info("You are not sitting at any table  !");
		} else if (msg instanceof SeatCommand) {
			lobby.tell(new SeatCommand(((SeatCommand)msg).tableName(), name), self());
		} else if(msg instanceof YouAreSeatedEvent) {
			persist(msg, event -> {
				log.info("You just seated on the selected table!");
				table = ((YouAreSeatedEvent)event).table();
				table.tell(new SeatConfirmation(name), self());
				getContext().become(seated);
			});
		} else if(msg instanceof GetAllTablesCommand) {
			lobby.tell(msg, self());
		} else if(msg instanceof AllTablesListings) {
			log.info("tables: " + ((AllTablesListings)msg).tables().stream().map(e -> "( " + e.name() + " - " + e.seatedPlayers() + " ) ").reduce("", String::concat));
		} else {
			commonHandler(msg);
		}
	}

	@Override
	public void onReceiveRecover(Object event) throws Throwable {
		if (event instanceof WonEvent) {
			money += ((WonEvent)event).amount();
			lobby.tell(new MoneyAdjustedEvent(name, money), self());
			log.info("You won {} from your bet on {}! You have {} money left", ((WonEvent)event).amount(),((WonEvent)event).bet().ammount(), money);
		} else if (event instanceof BetAcceptedEvent){
			money -= ((BetAcceptedEvent)event).bet().ammount();
			lobby.tell(new MoneyAdjustedEvent(name, money), self());
			log.info("Your bet was accepted! You have {} money left", money);
		} else if (event instanceof YouAreSeatedEvent) {
			log.info("You just seated on the selected table!");
			table = ((YouAreSeatedEvent)event).table();
			table.tell(new SeatConfirmation(name), self());
			getContext().become(seated);
		} else if (event instanceof YouAreUnSeatedEvent) {
			log.info("You are back at the lobby!");
			table.tell(new UnSeatConfirmation(name), self());
			table = null;
			getContext().unbecome();
		} else {
			unhandled(event);
		}
	}
}
