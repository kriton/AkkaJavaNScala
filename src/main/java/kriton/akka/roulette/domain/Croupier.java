package kriton.akka.roulette.domain;

import static akka.japi.pf.ReceiveBuilder.match;

import java.util.concurrent.TimeUnit;

import kriton.akka.roulette.messages.BallLandedEvent;
import kriton.akka.roulette.messages.GetWinningBetsCommand;
import kriton.akka.roulette.messages.PickBallCommand;
import kriton.akka.roulette.messages.SpinCommand;
import kriton.akka.roulette.messages.WinnningBets;
import kriton.akka.roulette.messages.WonEvent;
import scala.concurrent.duration.FiniteDuration;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public class Croupier extends AbstractLoggingActor {

	private Croupier(ActorRef table, ActorRef wheel) {
		log().info("Croupier is ready!");
		
		wheel.tell(new PickBallCommand(), self());
		
		context().system().scheduler().schedule(new FiniteDuration(5L, TimeUnit.SECONDS),
				new FiniteDuration(35L, TimeUnit.SECONDS), wheel, 
				new SpinCommand(), context().system().dispatcher(), self());
		
		receive(
			match(BallLandedEvent.class, event -> {
				table.tell(new GetWinningBetsCommand(event.symbol()), self());
				context().system().scheduler().scheduleOnce(
						new FiniteDuration(5L, TimeUnit.SECONDS), wheel, 
						new PickBallCommand(), context().system().dispatcher(), self());
		}).
			match(WinnningBets.class, event -> {
				event.bets().forEach(bet -> bet.better().tell(new WonEvent(bet.ammount() * 36, bet), self()));
		}).
			matchAny(this::unhandled).build());
	}

	public static Props props(ActorRef table, ActorRef wheel) {
		return Props.create(Croupier.class, () -> new Croupier(table, wheel));
	}
}