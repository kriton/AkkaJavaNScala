package kriton.akka.roulette.domain;

import static akka.japi.pf.ReceiveBuilder.match;
import static java.lang.System.currentTimeMillis;

import java.util.LinkedHashMap;
import java.util.Map;

import kriton.akka.roulette.messages.LeaderBoardTable;
import kriton.akka.roulette.messages.MoneyAdjustedEvent;
import kriton.akka.roulette.messages.PrintCommand;
import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.japi.Pair;

public class Leaderboard extends AbstractLoggingActor {

	private final int timeoutInMillis;
	
	Map<String, Pair<Integer, Long>> playersMoney = new LinkedHashMap<>();

	private Leaderboard(int timeoutInSeconds) {
		this.timeoutInMillis = timeoutInSeconds * 1000;
		
		log().info("Leaderboard is ready!");

		receive(match(MoneyAdjustedEvent.class, event -> {
			playersMoney.put(event.name(), new Pair<>(event.ammount(), currentTimeMillis()));
		}).match(PrintCommand.class, command -> {
			cleanUp();
			sender().tell(new LeaderBoardTable(playersMoney.toString()), self());
		}).matchAny(this::unhandled).build());		
	}
	
	private void cleanUp() {
		playersMoney = playersMoney
				.entrySet()
				.stream()
				.filter(entry -> currentTimeMillis()
						- entry.getValue().second() < timeoutInMillis)
				.sorted((e1, e2) -> e2.getValue().first().compareTo(e1.getValue().first()))
				.collect(() -> new LinkedHashMap<>(),
						(t, a) -> t.put(a.getKey(), a.getValue()),
						(a, b) -> a.putAll(b));
	}

	public static Props props(int timeoutInSeconds) {
		return Props.create(Leaderboard.class, () -> new Leaderboard(timeoutInSeconds));
	}
}
