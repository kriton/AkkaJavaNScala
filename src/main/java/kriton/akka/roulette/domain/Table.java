package kriton.akka.roulette.domain;

import static akka.japi.pf.ReceiveBuilder.match;
import static java.lang.String.format;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import kriton.akka.roulette.messages.Bet;
import kriton.akka.roulette.messages.BetAcceptedEvent;
import kriton.akka.roulette.messages.BetRejectedEvent;
import kriton.akka.roulette.messages.BettingFinishedEvent;
import kriton.akka.roulette.messages.GetWinningBetsCommand;
import kriton.akka.roulette.messages.NewGameStartingEvent;
import kriton.akka.roulette.messages.NewTableEvent;
import kriton.akka.roulette.messages.SeatCommand;
import kriton.akka.roulette.messages.SeatConfirmation;
import kriton.akka.roulette.messages.TableDetails;
import kriton.akka.roulette.messages.TableDetailsRequest;
import kriton.akka.roulette.messages.TableNotificationEvent;
import kriton.akka.roulette.messages.UnSeatConfirmation;
import kriton.akka.roulette.messages.UnseatCommand;
import kriton.akka.roulette.messages.WinningNumber;
import kriton.akka.roulette.messages.WinnningBets;
import kriton.akka.roulette.messages.YouAreSeatedEvent;
import kriton.akka.roulette.messages.YouAreUnSeatedEvent;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;

// read https://opencredo.com/improved-akka-java-8/

public class Table extends AbstractLoggingActor {

	private boolean acceptingBets = true;

	List<Bet> bets = new ArrayList<>();
	Map<String, ActorRef> seatedPlayers = new HashMap<>();

	private Table(ActorRef lobby, String name) {
		lobby.tell(new NewTableEvent(name), self());
		log().info("Table is ready!");
		
		receive(
			match(SeatCommand.class, command -> {
				sender().tell(new YouAreSeatedEvent(self()), self());
			}).
			match(UnseatCommand.class, command -> {
				sender().tell(new YouAreUnSeatedEvent(), self());
			}).
			match(SeatConfirmation.class, event -> {
				seatedPlayers.values().forEach(p -> p.tell(new TableNotificationEvent(format("Player %s just seated on the table!", event.playerName())), self()));
				seatedPlayers.put(event.playerName(), sender());
			}).
			match(UnSeatConfirmation.class, event -> {
				if (seatedPlayers.remove(event.name()) != null) {
					seatedPlayers.values().forEach(p -> p.tell(new TableNotificationEvent(format("Player %s just left the table!", event.name())), self()));
				}
			}).
			match(Bet.class, bet -> {	
				if (acceptingBets) {
					seatedPlayers.values().forEach(p -> p.tell(new TableNotificationEvent(format("Bet (%s on %s) placed by %s", bet.ammount(), bet.symbol(), bet.name())), self()));
					bets.add(bet);
					sender().tell(new BetAcceptedEvent(bet), self());
				} else 
					sender().tell(new BetRejectedEvent(bet), self());
			}).
			match(GetWinningBetsCommand.class, command -> {
				sender().tell(new WinnningBets(bets.stream().filter(bet -> bet.symbol() == command.symbol()).collect(Collectors.toSet())), self());
			}).
			match(NewGameStartingEvent.class, event -> {
				bets.clear();
				seatedPlayers.values().forEach(p -> p.tell(new TableNotificationEvent("You can now place bets!"), self()));
				log().info("{} is now accepting bets.", name);
				acceptingBets = true;
			}).
			match(BettingFinishedEvent.class, event -> {
				log().info("{} is no longer accepting bets.", name);
				seatedPlayers.values().forEach(p -> p.tell(new TableNotificationEvent("Betting is closed!"), self()));
				acceptingBets = false;
			}).
			match(WinningNumber.class, event -> {
				seatedPlayers.values().forEach(p -> p.tell(new TableNotificationEvent(format("Ball dropped in a pocket! Winning Number is: %s", event.number())), self()));
			}).
			match(TableDetailsRequest.class, request -> {
				sender().tell(new TableDetails(name, seatedPlayers.size(), self()), self());
			}).
			matchAny(this::unhandled).build()
		);		
	}

	public static Props props(ActorRef lobby, String name) {
		return Props.create(Table.class, () -> new Table(lobby, name));
	}
}
