package kriton.akka.roulette.domain;

import static akka.japi.pf.ReceiveBuilder.match;

import java.util.HashMap;
import java.util.Map;

import kriton.akka.roulette.messages.GetAllTablesCommand;
import kriton.akka.roulette.messages.NewTableEvent;
import kriton.akka.roulette.messages.SeatCommand;
import kriton.akka.roulette.queries.GetAllTablesQuery;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public class Lobby extends AbstractLoggingActor {

	private final Map<String, ActorRef> tables = new HashMap<>();
	
	private final ActorRef leaderboard = context().actorOf(Leaderboard.props(120), "leaderboard");

	private Lobby() {
		receive(match(SeatCommand.class, command -> {
			if (tables.containsKey(command.tableName()))
				tables.get(command.tableName()).forward(command, context());
		}).match(NewTableEvent.class, table -> {
			tables.put(table.name(), sender());
		}).match(GetAllTablesCommand.class, command -> {
			context().actorOf(GetAllTablesQuery.props(sender(), new HashMap<>(tables)));
		}).matchAny(message -> leaderboard.forward(message, context()))
		.build());
	}
	
	public static Props props() {
		return Props.create(Lobby.class, () -> new Lobby());
	}
}
