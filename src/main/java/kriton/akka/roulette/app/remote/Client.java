package kriton.akka.roulette.app.remote;

import java.util.concurrent.TimeUnit;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.FiniteDuration;
import kriton.akka.roulette.domain.Player;
import kriton.akka.roulette.domain.PlayerCommandInterpreter;
import kriton.akka.roulette.ui.ConsoleReader;
import akka.actor.ActorIdentity;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Identify;
import akka.pattern.AskableActorSelection;
import akka.util.Timeout;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Client {
	public static void main(String[] args) throws Exception {
		String playerName = "kriton" ; //args[0];
		Config sysConfig = ConfigFactory.load("player.app.conf");
		ActorSystem system = ActorSystem.create("player", sysConfig);
		Timeout timeout = new Timeout(new FiniteDuration(1000L, TimeUnit.SECONDS));
		
		AskableActorSelection asker = new AskableActorSelection(system.actorSelection("akka.tcp://game@127.0.0.1:2552/user/lobby"));		
		ActorRef lobby = ((ActorIdentity) Await.result(asker.ask(new Identify(1), timeout), timeout.duration())).getRef();


		ActorRef player = system.actorOf(Player.props(playerName, 100, lobby), playerName);
		ActorRef cI = system.actorOf(PlayerCommandInterpreter.props(player));
		system.actorOf(ConsoleReader.props(cI), "console");
	}
}
