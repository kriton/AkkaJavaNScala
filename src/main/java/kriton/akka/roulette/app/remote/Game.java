package kriton.akka.roulette.app.remote;

import kriton.akka.roulette.domain.Croupier;
import kriton.akka.roulette.domain.Lobby;
import kriton.akka.roulette.domain.Table;
import kriton.akka.roulette.domain.Wheel;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Game {
	public static void main(String[] args) {
		Config sysConfig = ConfigFactory.load("game.app.conf");
		ActorSystem system = ActorSystem.create("game", sysConfig);
		
		ActorRef lobby = system.actorOf(Lobby.props(), "lobby");

		ActorRef table1 = system
				.actorOf(Table.props(lobby, "table1"), "table1");
		ActorRef wheel1 = system.actorOf(Wheel.props(table1), "wheel1");
		ActorRef croupier1 = system.actorOf(Croupier.props(table1, wheel1),
				"James");

		ActorRef table2 = system
				.actorOf(Table.props(lobby, "table2"), "table2");
		ActorRef wheel2 = system.actorOf(Wheel.props(table2), "wheel2");
		ActorRef croupier2 = system.actorOf(Croupier.props(table2, wheel2),
				"Ben");
	}
}
