package kriton.akka.roulette.app.simple;

import static java.lang.String.format;

import java.security.SecureRandom;

import kriton.akka.roulette.domain.Croupier;
import kriton.akka.roulette.domain.Leaderboard;
import kriton.akka.roulette.domain.Lobby;
import kriton.akka.roulette.domain.Player;
import kriton.akka.roulette.domain.Table;
import kriton.akka.roulette.domain.Wheel;
import kriton.akka.roulette.domain.PlayerCommandInterpreter;
import kriton.akka.roulette.ui.ConsoleReader;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class App {
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		Config config = ConfigFactory.parseString("akka.loglevel = INFO \n"
				+ "akka.actor.debug.lifecycle = on");
		ActorSystem system = ActorSystem.create("Testing", config);

		ActorRef lobby = system.actorOf(Lobby.props(), "lobby");

		ActorRef table1 = system
				.actorOf(Table.props(lobby, "table1"), "table1");
		ActorRef wheel1 = system.actorOf(Wheel.props(table1), "wheel1");
		ActorRef croupier1 = system.actorOf(Croupier.props(table1, wheel1),
				"James");

		ActorRef table2 = system
				.actorOf(Table.props(lobby, "table2"), "table2");
		ActorRef wheel2 = system.actorOf(Wheel.props(table2), "wheel2");
		ActorRef croupier2 = system.actorOf(Croupier.props(table2, wheel2),
				"Ben");

		ActorRef player1 = system.actorOf(
				Player.props("Kriton", 100, lobby), "kriton");
		ActorRef player2 = system.actorOf(
				Player.props("Ed", 10, lobby), "Ed");
		ActorRef player3 = system.actorOf(
				Player.props("Tomas", 10, lobby), "Tomas");
		ActorRef p1UI = system.actorOf(PlayerCommandInterpreter.props(player1));
		ActorRef p2UI = system.actorOf(PlayerCommandInterpreter.props(player2));
		ActorRef p3UI = system.actorOf(PlayerCommandInterpreter.props(player3));

		p1UI.tell("show tables", p1UI);
		p1UI.tell("seat at table1", p1UI);
		p2UI.tell("seat at table1", p2UI);
		p3UI.tell("seat at table2", p3UI);

		Thread.sleep(500L);

		SecureRandom secureRandom = new SecureRandom();
		int i = 0;
		while (true) {
			p1UI.tell(format("bet 10 on %s", secureRandom.nextInt(37)), p1UI);
			p2UI.tell(format("bet 10 on %s", secureRandom.nextInt(37)), p2UI);
			p2UI.tell(format("bet 10 on %s", secureRandom.nextInt(37)), p2UI);
			if (i == 0) {
				p3UI.tell(format("bet 10 on %s", secureRandom.nextInt(37)), p3UI);
				p3UI.tell(format("bet 10 on %s", secureRandom.nextInt(37)), p3UI);
				p3UI.tell(format("bet 10 on %s", secureRandom.nextInt(37)), p3UI);
			} else if (i== 1) {
				p3UI.tell("unseat", p3UI);
			}else if (i== 2) {
				p3UI.tell("seat at table1", p3UI);
				p3UI.tell(format("bet 10 on %s", secureRandom.nextInt(37)), p3UI);
				p3UI.tell(format("bet 10 on %s", secureRandom.nextInt(37)), p3UI);
				p3UI.tell(format("bet 10 on %s", secureRandom.nextInt(37)), p3UI);
				system.actorOf(ConsoleReader.props(p3UI), "console");
			}
			Thread.sleep(5000L);
			p1UI.tell("leaderboard", p1UI);
			Thread.sleep(1000L);
			i++;
		}

	}
}
