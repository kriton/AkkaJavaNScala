package kriton.akka.roulette.queries;

import static akka.japi.pf.ReceiveBuilder.matchEquals;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import kriton.akka.roulette.messages.AllTablesListings;
import kriton.akka.roulette.messages.TableDetails;
import kriton.akka.roulette.messages.TableDetailsRequest;
import scala.concurrent.duration.FiniteDuration;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.PoisonPill;
import akka.actor.Props;

public class GetAllTablesQuery extends AbstractLoggingActor {

	private final Set<TableDetails> tableDetails = new HashSet<>();
	
	private GetAllTablesQuery(ActorRef requester, Map<String, ActorRef> tables) {

		Cancellable task = 
				context()
				.system()
				.scheduler()
				.scheduleOnce(new FiniteDuration(500L, TimeUnit.MILLISECONDS),
						self(), "terminate", context().system().dispatcher(),
						self());

		tables.forEach((name, ref) -> ref.tell(new TableDetailsRequest(),
				self()));

		receive(matchEquals("terminate", msg -> {
			self().tell(PoisonPill.getInstance(), self());
			sendResponse(requester);
		}).match(TableDetails.class, reply -> {
			tableDetails.add(reply);
			if (tableDetails.size() == tables.size()) {
				task.cancel();
				self().tell(PoisonPill.getInstance(), self());
				sendResponse(requester);
			}
		}).build());
	}

	private void sendResponse(ActorRef requester) {
		requester.tell(new AllTablesListings(tableDetails), context().parent());
	}

	public static Props props(ActorRef requester, Map<String, ActorRef> tables) {
		return Props.create(GetAllTablesQuery.class,
				() -> new GetAllTablesQuery(requester, tables));
	}
}
